#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using namespace std;

namespace adam
{

template <size_t idx, typename Head, typename... Tail>
struct tuple_impl : public tuple_impl<idx + 1, Tail...>
{
	using parent = tuple_impl<idx + 1, Tail...>;

	tuple_impl(const Head& p_h, const Tail&... types) :
		parent(types...),
		h(p_h)
	{}

	template <typename HeadAwi,
			  typename... TailAwi,
			  typename =
			  typename
			  std::enable_if<sizeof...(TailAwi) == sizeof...(Tail)>::type>
	tuple_impl(Head&& p_h, TailAwi&&... types) :
		parent(std::forward<Tail>(types)...),
		h(std::forward<Head>(p_h))
	{}

	tuple_impl(tuple_impl&& other) :
		parent(static_cast<tuple_impl::parent>(other)),
		h(other.h)
	{}

	Head h;
};

template <size_t idx, typename Head>
struct tuple_impl<idx, Head>
{
	tuple_impl(const Head& p_h) :
		h(p_h)
	{}

	template <typename HeadAwi,
			  typename =
			  typename enable_if<is_convertible<HeadAwi, Head>::value>::type>
	tuple_impl(HeadAwi&& p_h) :
		h(std::forward<HeadAwi>(p_h))
	{}

	tuple_impl(tuple_impl&& other)
	{}

	Head h;
};

template <typename... Types>
struct tuple : public tuple_impl<0, Types...>
{
	using parent = tuple_impl<0, Types...>;

	tuple(const Types&... types) :
		parent(types...)
	{}

	template <typename... TypesAwi,
			  typename =
			  typename
			  std::enable_if<is_convertible<TypesAwi, Types>::value...>::type>
	tuple(TypesAwi&&... types) :
		parent(std::forward<Types>(types)...)
	{}

	tuple(tuple&& other) = default;

	tuple& operator=(tuple&& other)
	{

	}
};

template <typename... Types>
tuple<Types&...> tie(Types... types)
{
	return tuple<Types&...>(types...);
}

} // namespace adam

int main()
{
	std::tuple<int, float> tt;
	int x;
	float y;
	adam::tuple<int, float> t(23, 56.0f);

	adam::tie(x, y) = t;

	return 0;
}
